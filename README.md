# Surface Pro 3 Linux Kernel 3.19.0-xx

From :
- https://github.com/neoreeps/surface-pro-3
- https://github.com/CarlosRoque/surface-pro-3-linux-kernel-3.19.0-27.29

## Install everything you need

```
sudo apt-get install git build-essential kernel-package fakeroot libncurses5-dev libssl-dev ccache kernel-wedge gawk
```

## Get the kernel

From [Ubuntu Kernel Git Guide](https://wiki.ubuntu.com/Kernel/Dev/KernelGitGuide)

### Ubuntu Vivid

```
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-vivid.git
```

### Ubuntu Wily

```
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-wily.git
```

### Ubuntu Xenial

```
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-xenial.git
```

## Apply the patches and Build

Copy or download patches within this repo and apply each of them with:

```
patch -p1 --ignore-whitespace -i {patch}
```

### Ubuntu Xenial

> **Note:** For Only Type Cover 3 v2 (045e:07e2) is taken into account for Ubuntu Xenial.

```
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-xenial.git
git clone git@framagit.org:spelle/ubuntu-surface-pro-3-kernel.git
cd ubuntu-xenial
patch -p1 --ignore-whitespace -i ../ubuntu-surface-pro-3-kernel/ubuntu-xenial/ubuntu-xenial_cameras.patch
patch -p1 --ignore-whitespace -i ../ubuntu-surface-pro-3-kernel/ubuntu-xenial/ubuntu-xenial_touchpad.patch
```

### Build the kernel

Build  new kernel with:

```
fakeroot debian/rules clean
DEB_BUILD_OPTIONS=parallel=4 AUTOBUILD=1 NOEXTRAS=1 fakeroot debian/rules binary-headers binary-generic
```

Or :

```
make-kpkg clean                                                                           
fakeroot make-kpkg --initrd --append-to-version=-surface-pro-3 kernel_image kernel_headers
```
